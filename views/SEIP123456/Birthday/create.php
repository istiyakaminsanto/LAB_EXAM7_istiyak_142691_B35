<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8">
    <title>login form</title>
    <link href="https://fonts.googleapis.com/css?family=Yellowtail" rel="stylesheet">
    <link rel="stylesheet" href="../../../resource/assets/css/bootstrap.css">
    <link rel="stylesheet" href="../../../resource/assets/css/bootstrap-responsive.css">
    <link rel="stylesheet" href="../../../resource/assets/css/style.css">

</head>
<body>
<div class="container">
    <div class="container-fluid">

        <header>
            <div class="logo_area">
                <p class="logo">Atomic Project</p>
                <h3 class="subLogo">SEIP-142691 Batch-35</h3>
            </div>
            <nav class="navbar navbar-inner">
                <ul class="nav">
                    <li><a href="../BookTitle/create.php">BOOKTITLE</a></li>
                    <li class="active"><a href="../Birthday/create.php">BIRTHDAY</a></li>
                    <li><a href="../Gender/create.php">GENDER</a></li>
                    <li><a href="../Email/create.php">EMAIL</a></li>
                    <li><a href="../Hobbies/create.php">HOBBIES</a></li>
                    <li><a href="../City/create.php">CITY</a></li>
                    <li><a href="../ProfilePicture/create.php">PROFILE PICTURE</a></li>
                    <li><a href="../SummaryOfOrganization/create.php">SUMMARY OF ORGANIZATION</a></li>
                </ul>

            </nav>
        </header>
        <div><img src="../../../resource/img/birthday.jpg" alt=""></div>
        <p class="title">Birthday</p>



        <?php
        require_once("../../../vendor/autoload.php");
        use App\Message\Message;

        if(!isset( $_SESSION)) session_start();
        echo Message::message();

        ?>

        <fieldset class="control-group ">
            <legend>Add your Date of Birth</legend>
            <form action="store.php" method="post" class="form-inline">
                <div class="input_form">
                    <label for="Date">Date of Birth&nbsp;</label>
                    <input type="date" id="Date" class="input-xxlarge" name="birthday">
                </div>
                <div class="input_form">
                    <label for="Author">Author Name</label>
                    <input type="text" id="Date" class="input-xxlarge" name="author_name">
                </div>
                <div>
                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
                    <input type="submit" id="Date" class="btn btn-success" value="submit">
                </div>
            </form>
        </fieldset>

    </div>
    <footer class="modal-footer">@copyright IstiyakAmin All data reserved </footer>
</div>
</body>
</html>