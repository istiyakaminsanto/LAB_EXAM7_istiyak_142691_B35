<?php
namespace App\ProfilePicture;
use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;


class ProfilePicture extends DB{
    public $id="";
    public $profile_picture="";
    public $author_name="";

    public function __construct(){
        parent:: __construct();
        if(!isset($_SESSION)) session_start();
    }

    public function setData($postVariableData=NULL){

        if(array_key_exists('id',$postVariableData)){
            $this->id = $postVariableData['id'];
        }

        if(array_key_exists('img_name',$postVariableData)){
            $this->profile_picture = $postVariableData['img_name'];
        }

        if(array_key_exists('author_name',$postVariableData)){
            $this->author_name = $postVariableData['author_name'];
        }
    }



    public function store(){

        $arrData = array( $this->profile_picture, $this->author_name);

        $sql = "Insert INTO profile_picture(profile_picture, author_name) VALUES (?,?)";
        $STH = $this->DBH->prepare($sql);

        $result = $STH->execute($arrData);

        if($result)
            Message::message("<h3>Success! Data Has Been Inserted Successfully :)</h3>");
        else
            Message::message("<h3>Failed! Data Has Not Been Inserted Successfully :( </h3>");

        Utility::redirect('create.php');


    }// end of store method







    public function index()
    {

    }




}

?>

